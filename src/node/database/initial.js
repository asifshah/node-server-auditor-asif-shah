( function(module) {
	var db               = require( 'src/node/database')();
	// var tables           = require( 'src/node/database/model');

	var initials         = require('configuration').initials;
	var requireDir       = require('require-dir');
	var path             = require('path');
	var promiseWhile 	 = require('src/node/com/report/snapShot/promiseWhile');
	var Q 				 = require('q');

	var appDir           = path.dirname(require.main.filename);
	var modulesPath      = appDir + '/data/';
	
	var newconfig        = {};
	var reverseSupported = {};
	var bulkData         = [];

	db.sync().then( function() {
		db.models.t_connection_type.findAndCountAll().then( function(result) {
			for(var key in initials.supported) {
				reverseSupported[initials.supported[key]] = key;
			}

			if ( result.count == 0 ) {
				for(var key in initials.supported) {
					bulkData.push({
						id_connection_type:   key,
						connection_type_name: initials.supported[key]
					});
				}

				return db.models.t_connection_type.bulkCreate(bulkData);
			}
			else
				return Q.resolve();
		}).then( function() {
			// let's fill the modules
			var reports 		= requireDir(modulesPath);
			var bulkData 		= [];

			for ( var key in reports ) {
				var listReports 	= [];

				reports[key].reports.forEach(function (reportName) {
					if( reportName.hasOwnProperty('reports')){
						reportName.reports.forEach(function (sectionRep) {
							repName = require(modulesPath + sectionRep);
							listReports.push( { rep: repName, relPath: sectionRep } );
						});
					}
					else {
						repName = require(modulesPath + reportName);
						listReports.push( { rep: repName, relPath: reportName } );
					}
				});

				bulkData.push( {
					module_name: key,
					id_connection_type: reverseSupported[ reports[key].type ],
					reports: listReports
				} );
			}

			return newconfig.saveModules( bulkData );
		}).then( function() {
			newconfig.connectionType = initials.supported;
			newconfig.reverseType = reverseSupported;
		} );;
	});

	newconfig.saveModules = function( bulkData ) {
		return db.query( "update t_module set is_active=0" ).then( function(result) {
			return db.query( "update t_query set is_active=0" );
		}).then( function() {
			var index = 0;
			return promiseWhile( function() {
				return index < bulkData.length
			}, function() {
				var defer = Q.defer();

				db.models.t_module.findOrCreate({
					where: {
						module_name: bulkData[index].module_name,
						id_connection_type: bulkData[index].id_connection_type
					}
				}).spread( function( module, created ) {
					var innerD = Q.resolve();

					if ( !created ) {
						module.is_active = true;
						innerD = module.save();
					}

					return innerD.then( function() {
						return newconfig.saveQuery(
							module.id_module,
							initials.supported[module.id_connection_type],
							bulkData[index].reports
						).then(function () {
							index ++;
							defer.resolve();
						});
					} );
				});

				return defer.promise;
			});
		}).finally( function() {
			return Q.resolve();
		});
	}

	newconfig.saveQuery = function( id_module, connectionType, reports ) {
		var index2 = 0;
		return promiseWhile( function() {
			return index2 < reports.length
		}, function() {
			var defer2 = Q.defer();

			var relpath = reports[index2].relPath.split( '/' );
			relpath.pop();

			db.models.t_query.findOrCreate( { where: {
				query_location: relpath.join( '/' ) + '/' + reports[index2].rep.file,
				report_name: reports[index2].rep.name,
				id_module: id_module
			} } ).spread( function( query, created ) {
				var innerD2 = Q.resolve();
				if ( !created ) {
					query.is_active = true;
					innerD2 = query.save();
				}

				return innerD2.then( function() {
					index2 ++;
					defer2.resolve();
				} );
			} );

			return defer2.promise;
		} );
	}

	module.exports = newconfig;

})(module);
