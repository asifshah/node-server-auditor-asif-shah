(function (module, Object) {
	var extend      = require('extend');
	var fs          = require('fs');
	var requireDir  = require('require-dir');
	var Data        = require('src/node/com/data');
	var path        = require('path');
	var initConfig  = require('src/node/database/initial');
	var appDir      = path.dirname(require.main.filename);
	var modulesPath = appDir + '/data/';
	var Q           = require('q');
	var db          = require('src/node/database')();

	var ReportModuleFactory = function (name) {
		ReportModuleFactory.getAvailable().then( function( available ) {
			if (available.indexOf(name) === -1) {
				throw 'no report modules defined';
			}
		} );

		this.name = name;

		var json = require(modulesPath + this.name);

		return new Data.ReportModule({
			name:        json.name,
			type:        json.type,
			description: json.description,
			reports:     mapReports(json.reports),
			repJson:     jsonMenu(json.reports)
		});

		function jsonMenu(reports){
			var report     = null;
			var collection = new Data.ReportCollection();
			var rows       = [];

			reports.forEach(function (sectionName) {
				if (sectionName.hasOwnProperty('name') && sectionName.hasOwnProperty('reports')) {
					row = {};

					row.reports = [];
					row.section = sectionName.name;

					sectionName.reports.forEach(function (reportName) {
						report = require(modulesPath + reportName);

						row.reports.push({ 'name' : report.name, pathSql: reportName, 'file': report.file });
					});

					rows.push(row);
				}
			});

			return rows;
		}

		function mapReports(reports) {
			var collection = new Data.ReportCollection();

			reports.forEach(function (reportName) {
				var reportArray = reportName.hasOwnProperty('reports') ? reportName.reports : [ reportName ];

				reportArray.forEach(function (sectionRep) {
					var report    = require(modulesPath + sectionRep);
					var reportDir = getReportDir(modulesPath + sectionRep);

					report.pathSql = sectionRep;

					collection.add(
						new Data.ReportItem(report,
							fs.readFileSync(reportDir + report.file, 'utf8'),
							report.show_query_file ? fs.readFileSync(reportDir + report.show_query_file, 'utf8') : null)
					);
				});
			});

			return collection;
		}

		function getReportDir(reportName) {
			var mask = /\/?(?:.(?!\/))+$/;

			return reportName.replace(mask, '/');
		}
	};

	ReportModuleFactory.getAvailable = function () {
		var reports = [];

		return db.models.t_module.findAll( {
			where: {
				is_active: true
			}
		}).then( function( data ) {
			data.forEach( function(val, idx) {
				reports.push( val.module_name );
			})

			return Q.resolve( reports );
		})

		//var reports = requireDir(modulesPath);
		//return Object.keys(reports);
	};

	ReportModuleFactory.getAvailableExtended = function () {
		var kvp    = {};
		var result = [];

		return db.models.t_query.findAll( {
			where: {
				is_active: true
			},
			include: [
				{
					model: db.models.t_module,
					include: [
						{ model: db.models.t_connection_type }
					]
				},
			]
		} ).then( function( data ) {
			data.forEach( function( val, idx ) {
				if ( kvp.hasOwnProperty( val.t_module.module_name ) ) {
					kvp[val.t_module.module_name].reports.push( val.report_name );
				}
				else {
					kvp[ val.t_module.module_name ] = {
						name:    val.t_module.module_name,
						type:    val.t_module.t_connection_type.connection_type_name,
						reports: [ val.report_name ],
					}
				}
			} );

			for( var p in kvp ) {
				result.push( kvp[p] );
			}

			return Q.resolve( result );
		} );
	};

	module.exports = ReportModuleFactory;
})(module, Object);
