(function(){
	var Data                = require('src/node/com/data');
	var ReportModuleFactory = require('src/node/com/report/reportModuleFactory');
	var Connector           = require('src/node/com/report/connector/');
	var SnapShot            = require('src/node/com/report/snapShot');
	var Filters             = require('src/node/com/report/filters');
	var Database            = require('src/node/database');
	var Q                   = require('q');
	var extend              = require('extend');
	var Promise             = require('promise' );
	var ServerConfiguration = require('src/node/com/report/serverConfiguration');
	var datamodels          = require('src/node/database/model');

	/**
	 * Report class
	 *
	 */
	function ReportManager() {
		/**
		 * configuration of the current report
		 */
		this.config = null;

		/**
		 * module of the current report
		 */
		this._module = null;

		/**
		 * connection of the current report
		 */
		this._server = null;

		/**
		 * module of the current report
		 */
		this._moduleFind = null;
	}

	/**
	 * Check if report is configured
	 *
	 * @returns {boolean}
	 */
	ReportManager.prototype.isConfigured = function () {
		return this.config instanceof Data.DB_Configuration;
	};

	/**
	 * Configure
	 *
	 * @param {Data.DB_Configuration} dbConf
	 * @returns {undefined}
	 */
	ReportManager.prototype.configure = function (dbConf) {
		if (dbConf instanceof Data.DB_Configuration) {
			this.config  = dbConf;
			this._module = null;

			return this.isConfigured();
		}

		if (dbConf === null) {
			this.config  = null;
			this._module = null;

			return true;
		}

		return false;
	};

	ReportManager.prototype.replaceParam11 = function(request, arrParam){
		for(var i = 0; i < arrParam.length; i++ ) {
			request = request.replace(arrParam[i].name, arrParam[i].value);
		}

		return request;
	};

	ReportManager.prototype.jsonMenu = function(){
		if (!this.isConfigured()) {
			return false;
		}

		var module  = this.module();
		var repJson = module.get('repJson')

		return repJson;
	}

	/**
	 * Get list of all reports
	 *
	 * @returns {undefined}
	 */
	ReportManager.prototype.list = function () {
		if (!this.isConfigured()) {
			return false;
		}

		var result = [];
		var module = this.module();

		module.get('reports').map(function (report) {
			result.push(report.get('name'));
		});

		return result;
	};

	/**
	 * Get specified report
	 *
	 * @param {string} name
	 * @param {string} recent
	 * @returns {object Q.Deferred}
	 */
	ReportManager.prototype.getReport = function (name, recent) {
		var prefs         = null;
		var server        = null;
		var connector     = null;
		var request       = null;
		var report        = null;
		var reportStorage = null;
		var queryShow     = null;
		var databaseName  = "";
		var module        = this.module();
		var dfd           = Q(null);

		recent = recent || false;

		if (!module) {
			return Q.reject({error: 'no report module required'});
		}

		if (!Connector[module.get('type')]) {
			return Q.reject({error: 'connector type not exists'});
		}

		report = module.get('reports').find(name);

		if (!report) {
			return Q.reject({error: 'report(' + name + ') not found'});
		}

		server = this.config.get('server');
		prefs  = {};

		extend(true, prefs, server.get('prefs'));

		request = report.get('request');
		reportStorage = Database(report.get('storage'));
		queryShow = report.get('queryShow');

		if (recent) {
			dfd = this.loadParamsFromReport(name, module, server, request)
			.then(function (result) {
				var tables;

				if(result){
					if('request' in result){
						request      = result.request;
						databaseName = result.databaseName;

						if(databaseName !== ""){
							prefs = replacePref(module.get('type'), prefs, databaseName);
						}
					}
				}

				connector = new Connector[module.get('type')](prefs);

				return connector.query(request).then(function (tablesData) {
					tables = Filters.validateDataSets(tablesData);

					var storePromise = SnapShot.set(reportStorage, {
						module:      module,
						name:        report.get('name'),
						primaryKeys: report.get('primaryKeys'),
						saveHistory: report.get('saveHistory'),
						serverName:  server.get('name')
					}, tables);

					// require promise fulfil only in case of historical reports (which depends on the data saved)
					return queryShow ? storePromise : tables;
				})
				.then(function () {
					return tables;
				}, function () {
					return tables;
				});
			})
			.catch(function (error) {
				console.log('error - ', error);
				return Q.reject(error);
			});
		}

		return dfd.then(function (tables) {
			var snapShotParams = {
				module:      module,
				name:        report.get('name'),
				primaryKeys: report.get('primaryKeys'),
				serverName:  server.get('name')
			};

			if (queryShow) {
				return SnapShot.queryShow(reportStorage, snapShotParams, queryShow);
			}

			if (tables) {
				return tables;
			}

			return SnapShot.get(reportStorage, snapShotParams).catch(function (err) {
				return [];
			});
		})
		.then(function (dataSet) {
			var data   = dataSet.slice(0);
			var result = new Data.ReportResult(report, data);

			return result.data();
		})
		.catch(function (err) {
			var result = new Data.ReportResult(report, err);

			return Q.reject(result.data());
		});

		function replacePref(type, pref, valueDatabase){
			switch(type){
				case 'mssql':
				case 'mysql':
					if('database' in pref){
						pref.database = valueDatabase;

						if('options' in pref){
							pref.options.database = valueDatabase;
						}
					}
					break;
			}

			return pref;
		}
	};

	/**
	 * Get specified report by scheduler
	 *
	 * @param {string} name
	 * @param {string} connType
	 * @param {string} connName
	 * @param {string} moduleType
	 * @param {string} recent
	 * @returns {object Q.Deferred}
	 */
	ReportManager.prototype.getReportByScheduler = function (name, connType, connName, moduleType, recent) {
		var prefs        = null;
		var connector    = null;
		var request      = null;
		var report       = null;
		var databaseName = "";
		var dfd          = Q.defer();
		var serverF      = this.server(connName);
		var module       = this.moduleFind(moduleType);

		recent = recent || false;

		if (!serverF) {
			dfd.reject({error: 'Connection name not exists'});
			return dfd.promise;
		}

		if (!module) {
			dfd.reject({error: 'no report module required'});
			return dfd.promise;
		}

		if (!Connector[connType]) {
			dfd.reject({error: 'connector type not exists'});
			return dfd.promise;
		}

		report = module.get('reports').find(name);

		if (!report) {
			dfd.reject({error: 'report(' + name + ') not found'});
			return dfd.promise;
		}

		prefs = {};

		extend(true, prefs, serverF.get('prefs'));

		request = report.get('request');

		if (recent) {
			this.loadParamsFromReport(name, module, serverF, request)
			.then(function (result) {
				if(result){
					if('request' in result){
						request      = result.request;
						databaseName = result.databaseName;

						if(databaseName !== ""){
							prefs = replacePref(module.get('type'), prefs, databaseName);
						}
					}
				}

				connector = new Connector[connType](prefs);

				connector.query(request).then(function (tables) {
					tables = Filters.validateDataSets(tables);

					success(tables);
				}, error);
			})
			.catch(function(error){
				console.log('error - ', error);
				dfd.reject(error);
			});
		} else {
			SnapShot.get(Database(report.get('storage')), {
				module:      module,
				name:        report.get('name'),
				primaryKeys: report.get('primaryKeys'),
				serverName:  serverF.get('name')
			}).then(function (table) {
				success(table, true);
			}, function () {
				success([], true);
			});
		}

		return dfd.promise;

		function success(dataSet, noRecord) {
			var data = [];

			if (!noRecord) {
				SnapShot.set(Database(report.get('storage')), {
					module:      module,
					name:        report.get('name'),
					primaryKeys: report.get('primaryKeys'),
					saveHistory: report.get('saveHistory'),
					serverName:  serverF.get('name')
				}, dataSet);
			}

			dataSet.forEach(function (records) {
				data.push(records);
			});

			var result = new Data.ReportResult(report, data);

			dfd.resolve(result.data());
		}

		function error(err) {
			var result = new Data.ReportResult(report, err);

			dfd.reject(result.data());
		}

		function replacePref(type, pref, valueDatabase){
			switch(type){
				case 'mssql':
				case 'mysql':
					if('database' in pref){
						pref.database = valueDatabase;

						if('options' in pref) {
							pref.options.database = valueDatabase;
						}
					}
					break;
			}

			return pref;
		}
	};

	ReportManager.prototype.module = function () {
		if (this._module === null) {
			if (this.isConfigured()) {
				this._module = ReportModuleFactory(this.config.get('module'));
			} else {
				return false;
			}
		}

		return this._module;
	};

	ReportManager.prototype.getServer = function () {
		if (this.isConfigured()) {
			return this.config.get('server');
		}

		return false;
	}

	/**
	* Find module by name
	*
	* @param {string} name
	*/
	ReportManager.prototype.moduleFind = function (name) {
		this._moduleFind = ReportModuleFactory(name);

		return this._moduleFind;
	};

	/**
	 * function get params select report
	 *
	 * nameRep
	 */
	ReportManager.prototype.loadParamsFromReport = function(nameRep, module, server, request) {
		var parameters = null;
		var aPromise   = [];
		var connType   = null;
		var connName   = null;
		var dfd        = Q.defer();
		var report     = null;

		if (!module) {
			return Q.reject({error: 'no report module required'});
		}

		connType   = module.get('type');
		connName   = server.get('name');
		report     = module.get('reports').find(nameRep);
		parameters = report.get('parameters');

		if (!parameters) {
			return Q(null);
		}

		parameters.forEach(function(param){
			aPromise.push( new Promise( function( resolve, reject ){
				datamodels.db_query_parameters.find({
					where:{
						connType:  connType,
						connName:  connName,
						repName:   nameRep,
						nameParam: param.name
					}
				}
				).then(function (record) {
					if(record){
						param.value = record.valueParam;
					}

					resolve(param);
				});
			}));
		});

		Promise.all( aPromise ).then( function( resp ){
			var jsonRow      = {};
			var resJson      = [];
			var nameParam    = "";
			var valueParam   = null;
			var databaseName = "";

			for(var i = 0; i < resp.length; i++){
				jsonRow = {};

				if(resp[i].database === 'true'){
					nameParam    = 'noParam';
					databaseName = resp[i].value;
				}
				else {
					nameParam = resp[i].name;
				}

				try {
					switch(resp[i].type.toLowerCase()){
						case 'string':
							valueParam = '\'' + resp[i].value + '\'';
							break;
						case 'integer':
							valueParam = parseInt(resp[i].value);
							break;
						case 'decimal':
							valueParam = parseFloat(resp[i].value);
							break;
						case 'date':
							valueParam = '\'' + (new Date(resp[i].value)).toDateString() + '\'';
							break;
						case 'datetime':
							valueParam = '\'' + (new Date(resp[i].value)).toString() + '\'';
							break;
						default:
							valueParam = '\'' + resp[i].value + '\'';
							break;
					}
				}
				catch(err) {
					valueParam = '\'' + resp[i].value + '\'';
				}

				request = request.replace(new RegExp(nameParam, 'g'), valueParam );
			}

			dfd.resolve({'request': request, 'databaseName': databaseName } );
		}, function( err ){
			dfd.reject(err);
		});

		return dfd.promise;
	};

	/**
	* Find server by name
	*
	* @param {string} name
	*/
	ReportManager.prototype.server = function (name) {
		this._server = ServerConfiguration.getConfiguration(name);

		return this._server;
	};

	module.exports = ReportManager;
})();
