(function (module) {
	var Q                  = require('q');
	var getModelFromDB     = require('./getModelFromDB');
	var getModelFromRecord = require('./getModelFromRecord');
	var modActs            = require('./modelActions');
	var async              = require('async');
	var extend             = require('extend');
	var md5                = require('md5');

	function generateStrHash(columns, record) {
		return md5(JSON.stringify(columns.map(function (colName) { return record[colName]; })));
	}

	module.exports = function (database, params, datasets) {
		params = params || {};

		var dfd     = Q.defer();
		var topName = params.module.get('type') + params.module.get('name') + params.name;

		database.sync().then(function () {
			return modActs.getConnection(params.serverName, false);
		}).then(function (connection) {
			datasets.forEach(function (datasetSrc, i) {
				var name          = topName + i;
				var serial        = 1;
				var datasetHashes = {};

				var dataset = datasetSrc.map(function (record) {
					var targetRec = {};

					extend(true, targetRec, record, {
						connectionId: connection.get('id')
					});

					if (params.primaryKeys) {
						targetRec.id = serial++;

						var recHash = generateStrHash(params.primaryKeys, record);

						if (recHash in datasetHashes) {
							// should be caught with dfd.reject
							throw new Error('Dataset contains rows with duplicated hashes');
						}

						targetRec['_hash'] = recHash;

						datasetHashes[recHash] = targetRec;
					}

					return targetRec;
				});

				getModelFromDB(database, name, params.primaryKeys).catch(function (err) {
					return null;
				})
				.then(function (ModelDB) {
					getModelFromRecord(database, name, datasetSrc, params.primaryKeys).then(function (recordModel) {
						var updateDfd = Q(true);

						if (ModelDB) {
							if (!modActs.compare(ModelDB, recordModel)) {
								updateDfd = modActs.destroy(ModelDB, {isAll: true});
							} else {
								if (!params.primaryKeys) {
									if (!params.saveHistory) {
										updateDfd = modActs.destroy(recordModel, {connection: connection});
									}
								} else {
									// "dataset" and its records would be modified within "mergeDataset"
									updateDfd = mergeDatasetIntoDB(recordModel, connection, params.primaryKeys, params.saveHistory, dataset, datasetHashes);
								}
							}
						}

						updateDfd.finally(function () {
							database.sync().then(function () {
								var promises = dataset.map(function (record) {
									return function (next) {
										recordModel.create(record).finally(next);
									};
								});

								async.auto(promises, function (err, results) {
									if (err) {
										dfd.resolve();
									} else {
										dfd.reject();
									}
								});
							});
						});
					}, dfd.reject);

					saveRecords( topName, params.module.get('type'), params.module.get('name'), i );
				});
			});
		}).catch(dfd.reject);

		function saveRecords( tablename, connectionType, module, recordset ) {
			var defaultDb        = require( 'src/node/database')();
			var tname            = tablename + recordset;
			var connectionTypeId = 0;
			var idModule         = 0;
			var queryId          = 0;

			defaultDb.models.t_query_result_table.findAll( { where: { report_table_name: tname } }).then( function( data ) { // on success
				if (data && data.length > 0)
				{
					return Q.reject("table already exists ");
				}

				return defaultDb.models.t_module.findAll( { where: { module_name: module + "." + connectionType } } );
			}, function( msg ) { // on reject
				console.log( msg );
			}).then( function( data ) { // on success
				if (!data || data.length != 1) {
					return Q.reject("Module doesn't exists or exists multiple instance.");
				}

				connectionTypeId = data[0].id_connection_type;
				idModule         = data[0].id_module;

				return defaultDb.models.t_query.findOne({
					where: {
						report_name: params.name,
						id_module:   idModule
					}
				});
			}).then( function( qry ) {
					if (!qry) {
						Q.reject( "No such Query" );
					}

					return defaultDb.models.t_query_result_table.create({
						id_recordset:       recordset,
						report_table_name:  tname,
						t_module_id:        idModule,
						id_connection_type: connectionTypeId,
						id_query:           qry.id_query
					});
				}, function( msg ) { // on reject
					console.log( msg );
				}
			).then( function( ) {
				// console.log( 'successfully recorded' );
			}, function( err ) {
				console.log( err );
			} );
		}

		return dfd.promise;
	};

	function mergeDatasetIntoDB(Model, connection, primaryKeys, saveHistory, dataset, datasetHashes) {
		var allKeysAndHash = primaryKeys.concat([ 'connectionId', '_hash' ]);

		return Model.findAll({
			attributes: allKeysAndHash, // we have to fetch all the primary keys for record if we wish to update it later on
			where: { connectionId: connection.get('id') }
		}).then(function (result) {
			var promises = [];

			result.forEach(function (row) {
				var rowHash = row.get('_hash');

				if (rowHash in datasetHashes) {
					var record = datasetHashes[rowHash];

					delete datasetHashes[rowHash];

					allKeysAndHash.forEach(function (colName) {
						delete record[colName];
					});

					promises.push(function (next) {
						row.update(record).finally(next);
					});
				} else {
					if (!saveHistory) {
						promises.push(function (next) {
							row.destroy().finally(next);
						});
					}
				}
			});

			dataset.length = 0;

			for(var recHash in datasetHashes) {
				dataset.push(datasetHashes[recHash]);
			}

			var dfd = Q.defer();

			async.auto(promises, function (err, results) {
				if (err) {
					dfd.resolve();
				} else {
					dfd.reject();
				}
			});

			return dfd;
		});
	}
})(module);
