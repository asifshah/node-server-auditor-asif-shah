(function (module) {
	var dbsched      = require('src/node/database/model');
	var Report       = require('src/node/com/report');
	var database     = require('src/node/database');
	var nodeSchedule = require('node-schedule');
	var async        = require('async');
	var Q             = require('q');
	var Promise       = require('promise');

	/**
	 * Report class
	 *
	 */
	function Scheduler() {
		this._arr_sched = [];
	}

	Scheduler.prototype.enabledSched = function(record, sessionId) {
		var active_schedule = null;
		var recent          = true;
		var id_record       = record.id;
		var repName         = record.repName;
		var connType        = record.connType;
		var connName        = record.connName;
		var moduleType      = record.moduleType;
		var schedule        = record.schedule;
		var dfd             = Q.defer();
		var timeSched       = schedule;

		var nameJob = this.getCurrNameJob(record);

		active_schedule = nodeSchedule.scheduleJob(nameJob, timeSched, function(){
			Report(sessionId).getReportByScheduler(repName, connType, connName, moduleType, recent)
			.then(function (result) {
				dfd.resolve(result);
			})
			.catch(function(error){
				dfd.reject(err);
			});
		});

		this.updateSelector(record, true)
			.then( function (result) {
				dfd.resolve(result);
			})
			.catch(function(error){
				dfd.reject(err);
			});

		return dfd.promise;
	};

	Scheduler.prototype.disabledSched = function(record, sessionId) {
		var id_record    = record.id;
		var dfd          = Q.defer();
		var find_i       = -1;
		var stopJob      = null;
		var arrJobs      = this.getStartedJobs();
		var currNameStop = this.getCurrNameJob(record);

		if (arrJobs.hasOwnProperty(currNameStop) ) {
			stopJob = arrJobs[currNameStop];

			stopJob.cancel();

			this.updateSelector(record, false)
			.then( function (result) {
				dfd.resolve(result);
			})
			.catch(function(error){
				dfd.reject(err);
			});
		}
		else {
			dfd.reject({'result': 'not found'});
		}

		return dfd.promise;
	};

	Scheduler.prototype.getCurrNameJob = function(record) {
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	Scheduler.prototype.getStartedJobs = function(){
		return nodeSchedule.scheduledJobs;
	}

	Scheduler.prototype.list = function () {
		var tables = [];
		var dfd    = Q.defer();

		dbsched.dbSettScheduler.findAll({})
			.then( function(table) {
				var result = table.map(extractItem);

				if (result) {
					tables.push(result);
				}

				dfd.resolve(tables);
			})
			.finally(function(){
				dfd.resolve(tables);
			});

		return dfd.promise;

		function extractItem(item) {
			var dataValues = item.dataValues;
			var resItem    = {};

			for (var key in dataValues) {
				if (dataValues.hasOwnProperty(key)) {
					resItem[key] = dataValues[key];
				}
			}

			return resItem;
		};
	};

	Scheduler.prototype.insert = function(record){
		var dfd = Q.defer();

		dbsched.dbSettScheduler.sync().then(function () {
			var promises = [];

			promises.push( new Promise( function( resolve, reject ) {
				dbsched.dbSettScheduler.create({
					connType:   record.connType,
					connName:   record.connName,
					moduleType: record.moduleType,
					repName:    record.repName,
					schedule:   record.schedule,
					isEnabled:  record.isEnabled
				})
				.then( function(res) {
					resolve(res);
				})
				.catch(function(err){
					reject(err);
				});
			}));

			Promise.all( promises ).then( function( resp ){
				dfd.resolve(resp);
			}, function( err ){
				dfd.reject(err);
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.updateSelector = function(record, selector){
		var dfd = Q.defer();

		dbsched.dbSettScheduler.sync().then(function () {
			var promises = [];

			promises.push(function (next) {
				dbsched.dbSettScheduler.find( { where:{ id :  record.id } })
					.then(function (record){
						if(record){
							record.updateAttributes({
								isEnabled: selector
							}).catch(function(err){
								dfd.reject(err);
							});
						}
					}).finally(next);;
			});

			async.auto(promises, function (err, results) {
				if (err) {
					dfd.reject(err);
				} else {
					dfd.resolve(results);
				}
			});
		});

		return dfd.promise;
	};

	/*
	 * a storage for all schedulers
	 */
	var scheduler_instances = { };

	/*
	 * a factory for report instances
	 */
	module.exports = function (sessionId) {
		if (!(sessionId in scheduler_instances)) {
			scheduler_instances[sessionId] = new Scheduler();
		}

		return scheduler_instances[sessionId];
	};
})(module);
