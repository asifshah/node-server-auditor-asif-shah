(function (module) {
	var handler   = null;
	var express   = require('express');
	var router    = express.Router();
	var scheduler = require('src/node/com/scheduler');

	router.post('/api/enabled', function (req, res) {
		var record    = req.body;
		var sessionId = req.sessionId;

		scheduler(req.sessionId).enabledSched(record, sessionId ).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			// console.log('error enabled  - ', err);
			res.json(handler.error('scheduler enabled error', err));
		});
	});

	router.post('/api/disabled', function (req, res) {
		var record = req.body;
		var sessionId = req.sessionId;

		scheduler(req.sessionId).disabledSched(record, sessionId ).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('scheduler disabled error', err));
		});
	});

	router.get('/api/allsched', function (req, res) {
		scheduler(req.sessionId).list().then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('Error get list schedulers', err));
		});
	});

	router.put('/api/insert', function (req, res) {
		var record = req.body;

		scheduler(req.sessionId).insert(record).then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('Error insert record', err));
		});
	});

	router.get('/api/arr', function (req, res) {
		var record = req.body;

		scheduler(req.sessionId).getSrart().then(function (result) {
			res.json(handler.response(result));
		}, function (err) {
			res.json(handler.error('Error get array start scheduler', err));
		});
	});

	module.exports = function (resHandler) {
		handler = resHandler;
		return router;
	};
})(module);
