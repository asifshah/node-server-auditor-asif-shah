/* global angular, app */

/**
 * Date Line Component
 *
 * @requires BaseGraphComp
 * @requires Collection
 * @requires DateLineData
 */
app.factory('DateLineComp', ['BaseGraphComp', 'Collection', 'DateLineData', function (BaseGraphComp, Collection, DateLineData) {
	var DateLineComp = function () {
		BaseGraphComp.call(this);

		angular.merge(this.options, {
			axes: {
				xaxis: {
					renderer: $.jqplot.DateAxisRenderer
				}
			},
			series: [
				{
					lineWidth: 2,
					markerOptions: {
						show: true,
						style: 'square',
						lineWidth: 2
					}
				}
			]
		});
	};

	angular.extend(DateLineComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new Collection(DateLineData, data);

			return [graphLine.data()];
		}
	});

	return DateLineComp;
}]);
