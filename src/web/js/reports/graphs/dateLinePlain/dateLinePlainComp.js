/* global angular */

/**
 * Date Line Plain Component
 *
 * @requires DateLineComp
 * Dependent from DateLine Graph
 */
app.factory('DateLinePlainComp', ['DateLineComp', function (DateLineComp) {
	var DateLinePlainComp = function () {
		DateLineComp.call(this);

		angular.merge(this.options, {
			series: [
				{
					markerOptions: {
						show: false
					}
				}
			]
		});
	};

	angular.extend(DateLinePlainComp.prototype, DateLineComp.prototype, {
	});

	return DateLinePlainComp;
}]);
