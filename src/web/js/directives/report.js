app.directive('report', ['$timeout', '$filter', function ($timeout, $filter) {
	return {
		restrict:    'E',
		templateUrl: 'directives/report.html',
		replace:     true,
		scope: {
			reportData:      '=',
			reportErrorText: '=',
			refreshReport:   '&',
			reloadReport:    '&'
		},
		link: function ($scope, element, attrs) {
			var firstAct = true;

			$scope.$watch('reportData', function (oldValue, newValue) {
				if ($scope.reportData !== undefined) {
					$scope.multiSets = $scope.reportData.result.length > 1;

					if ($scope.reportData.component.struct) {
						transformReportResult($scope.reportData.result, $scope.reportData.component.struct);
					}
				}

				$scope.reportError = !!$scope.reportErrorText;
			});

			firstAct = true;

			$scope.activateTab = function (item) {
				/*
				 * Wait until tab rendering
				 */
				if (!firstAct) {
					$timeout(function () {
						$scope.$broadcast('replot.uiChart', item);
					}, 30);
				}

				firstAct = false;
			}

			function transformReportResult(reportResult, struct) {
				reportResult.map(function (set) {
					set.struct = struct;

					set.map(function (item) {
						var key;

						for (key in struct) {
							if (struct.hasOwnProperty(key)
									&& struct[key].source in item) {
								item[key] = formatCell(item[struct[key].source], struct[key].format, struct[key].type);
							}
						}

						for (key in item) {
							if (item.hasOwnProperty(key) && !(key in struct)) {
								delete item[key];
							}
						}
					});
				});

				function formatCell(cell, format, type) {
					var result;

					if (!format) {
						return cell;
					}

					switch (type) {
						case 'date':
							result = $filter('date')(cell, format);
							break;
						default:
							result = cell.match(new RegExp(format, 'g'));
							result = (result !== null) ? result.join('') : '';
					}

					return result;
				}
			}
		}
	};
}]);
