(function (app) {
	app.controller('schedCreateCtrl', ['$scope', '$query', '$uibModalInstance', '$window', function ($scope, $query, $uibModalInstance, $window) {
		var getTypeByModuleName;

		$scope.modules    = [];
		$scope.servers    = [];
		$scope.reports    = [];
		$scope.moduleType = null;
		$scope.connName   = null;
		$scope.connType   = null;
		$scope.repName    = null;
		$scope.schedule   = '';

		$scope.close = function () {
			$uibModalInstance.dismiss('close');
		};

		$scope.submit = function () {

		var str_schedule = getStrSchedule();

		var record = {
			moduleType : $scope.moduleType,
			connName   : $scope.connName,
			connType   : $scope.connType,
			repName    : $scope.repName,
			schedule   : str_schedule,
			isEnabled  : 0,
			ctEnabled  : false
		};

		$query('scheduler/api/insert', record, 'PUT' ).then(function (result) {
			var retRecord = result.response[0];
	
			retRecord.ctEnabled = false;
	
			$uibModalInstance.close(retRecord);

		}, function (res) {
			$scope.mess = res.statusText;
		});
		};

		getStrSchedule = function(){
			return ($scope.schedule === '') ? '* * * * * 1' : $scope.schedule;
		}

		getTypeByModuleName = function (name) {
			for(var i = 0; i < $scope.modules.length; i++){
				if($scope.modules[i].name === name){
					return $scope.modules[i].type;
				}
			}
		};

		replaceStructReports = function(){
			var nameModule;

			$scope.modules.forEach(function (item) {
				nameModule = item.name;

				item.reports.forEach(function (report) {
					$scope.reports.push( { module: nameModule, name: report } );
				});
			});
		}

		$query('config/reports').then(function (data) {
			var res = data.response;

			$scope.modules = res.modules;
			$scope.servers = res.servers;

			replaceStructReports();

			$scope.connType   = $scope.modules[0].type
			$scope.moduleType = $scope.modules[0].name;
			$scope.connName   = $scope.servers[0];
			$scope.repName    = $scope.modules[0].reports[0];
		}, function () {
		});
	}]);
})(app);
