(function (app) {
	app.controller('schedulerCtrl', ['$scope', '$query', '$translate', '$uibModal', function ($scope, $query, $translate, $uibModal) {
		// Load info scheduler
		$scope.dataSched = [];

		$scope.LoadInfoScheduler = function () {
			$scope.dataSched = [];

			$query('scheduler/api/allsched', {}).then(function (res) {
				var data = [];

				if(res.response.length === 0){
					throw new Error('data not found');
				}

				data = res.response[0];

				for(var i = 0; i < data.length; i++ ){
					data[i].ctEnabled = (data[i].isEnabled) ? true : false;
				}

				$scope.dataSched = data;
			})
			.catch(function(error){
			});
		}

		$scope.openAddItemSchedule = function () {
			var modalInstance = $uibModal.open({
				//animation:   true,
				templateUrl: '/pages/schedCreate.html',
				controller:  'schedCreateCtrl'
			});

			modalInstance.result.then(function (result) {
				if(result){
					$scope.dataSched.push(result);
				}

			}, function () {
			});
		};

		$scope.isEnabled = function (record) {
			if(record.ctEnabled == true){
				$query('scheduler/api/disabled', record, 'POST' ).then(function (data) {
					changeFlag(record.id, false);
				}, function (res) {
					// console.log('res disabled:  ', res);
				})
				.catch(function(error){
					// console.log('Error: ', error);
				});
			}
			else {
				$query('scheduler/api/enabled', record, 'POST' ).then(function (data) {
					// console.log('enabled scheduler', data);
					changeFlag(record.id, true);
				}, function (res) {
					// console.log('res enabled: ', res);
				})
				.catch(function(error){
					// console.log('Error: ', error);
				});
			}
		}

		function changeFlag(id_record, flag){
			for(var i = 0; i < $scope.dataSched.length; i++ ){
				if($scope.dataSched[i].id === id_record){
					$scope.dataSched[i].ctEnabled = flag;
					break;
				}
			}
		}
	}]);
})(app);
